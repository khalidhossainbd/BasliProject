<?php include('partials/header.php');?>
<?php include('partials/navber.php');?>
	

	<section>
		<div class="container">
		<div id="myCarousel" class="carousel slide">
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#myCarousel" data-slide-to="1"></li>
		    <li data-target="#myCarousel" data-slide-to="2"></li>
		  </ol>
		  <div class="carousel-inner">
		    <div class="item active">
		      <img src="dist/images/slider/slider-01.jpg" class="img-responsive">
		      <div class="container">
		        <div class="carousel-caption">
		          <h1 class="animated fadeInLeft">Change system not deaf child.</h1>
		          <p>Committed to the development of the profession of sign language 
						interpreting worldwide</p>
		          <p></p>
		        </div>
		      </div>
		    </div>
		    <div class="item">
		      <img src="dist/images/slider/slider-02.jpg" class="img-responsive">
		      <div class="container">
		        <div class="carousel-caption">
		          <h1 class="animated slideInDown">Change system not deaf child.</h1>
		          <p>Committed to the development of the profession of sign language 
						interpreting worldwide</p>
		          <p></p>
		        </div>
		      </div>
		    </div>
		    <div class="item">
		      <img src="dist/images/slider/slider-04.jpg" class="img-responsive">
		      <div class="container">
		        <div class="carousel-caption">
		          <h1 class="animated zoomInUp">Change system not deaf child.</h1>
		          <p>Committed to the development of the profession of sign language 
						interpreting worldwide</p>
		          <p></p>
		        </div>
		      </div>
		    </div>
		  </div>
		  <!-- Controls -->
		 <!--  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		    <span class="icon-prev"></span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" data-slide="next">
		    <span class="icon-next"></span>
		  </a> -->  
		</div>
		</div>
	</section>

	<section style="margin: 50px;">
		<div class="container">
			<div class="row">
				
				<div class="col-md-8">
					<p class="welcome">Welcome</p>
					<hr class="wel-hr">
					<p class="wel-text">Basli committed to the development of the profession of sign languae interpreting and sign language support to deaf peoples.We develop and promote standards for high quality training, education and assessment of sign language interpreters</p>
				</div>
				<div class="col-md-4">
					<div class="main-img animated fadeInLeft">
					<img class="img-responsive" src="dist/images/mamun5.jpg">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section style="margin: 50px;">
		<div class="container">
			<div class="">
				<p class="welcome">Our Services</p>
				<hr class="wel-hr">
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="serve-blog animated fadeInLeft">
						<h4>Training</h4>
						<hr class="hr-class">
						<p>Basli committed to the development of the profession of sign languae interpreting and sign language support to deaf peoples.</p>
						<img class="img-responsive" src="dist/images/mamun01.jpg">
					</div>
				</div>
				<div class="col-md-4">
					<div class="serve-blog animated fadeInUp">
						<h4>Consultancy</h4>
						<hr class="hr-class">
						<p>Basli committed to the development of the profession of sign languae interpreting and sign language support to deaf peoples.</p>
						<img class="img-responsive" src="dist/images/mamun03.jpg">
					</div>
				</div>
				<div class="col-md-4">
					<div class="serve-blog animated fadeInRight">
						<h4>Research & References</h4>
						<hr class="hr-class">
						<p>Basli committed to the development of the profession of sign languae interpreting and sign language support to deaf peoples.</p>
						<img class="img-responsive" src="dist/images/mamun02.jpg">
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="container">
			<div style="margin-bottom: 50px;">
				<p class="welcome">Some Memories</p>
				<hr class="wel-hr">
			</div>
		</div>
		<div class="container-fluid" style="background-color: #76ab9c; padding: 40px;">
			<div class="row">

			<!--01-->
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery01.jpg" class="img-responsive" />
				
				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Bangladesh Assocation of Sign Language Interpreters</p>
				</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery02.jpg" class="img-responsive" />

				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Bangladesh Assocation of Sign Language Interpreters</p>
				</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery03.jpg" class="img-responsive" />

				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Bangladesh Assocation of Sign Language Interpreters</p>
				</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery04.jpg" class="img-responsive" />

				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Bangladesh Assocation of Sign Language Interpreters</p>
				</div>
				</div>
			</div>

			</div>

			<div class="row">
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery05.jpg" class="img-responsive" />
				
				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Bangladesh Assocation of Sign Language Interpreters</p>
				</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery06.jpg" class="img-responsive" />

				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Bangladesh Assocation of Sign Language Interpreters</p>
				</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery07.jpg" class="img-responsive" />

				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Bangladesh Assocation of Sign Language Interpreters</p>
				</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 col-xs-12">
				<div class="product-list-left-effect">
				<img src="dist/images/gallery/gallery08.jpg" class="img-responsive" />
				<div class="product-overlay">
				<h3>Sign Language Interpreters</h3>
				<p>Cras pharetra lorem a arcu cursus consequat. Vivamus aliquet molestie tellus, porta</p>
				</div>
				</div>
			</div>

			</div>
		</div>
	</section>


	<section id="carousel">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
	                <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
					<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
					  <!-- Carousel indicators -->
	                  <ol class="carousel-indicators">
					    <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
					    <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
					    <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
	                    <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
	                    <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
	                    <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
					  </ol>
					  <!-- Carousel items -->
					  <div class="carousel-inner">
					    <div class="item">
					    	<div class="profile-circle" style="background-color: rgba(77,5,51,.2);">
					    		<img class="img-thumbnail" src="dist/images/mamun01.jpg" style="height: 100px; width: 100px; border-radius: 50px;">
					    	</div>
	                        	
	                        
					    	<blockquote>
					    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
					    	</blockquote>	
					    </div>
					    <div class="item">
	                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);">
	                        	<img class="img-thumbnail" src="dist/images/mamun01.jpg" style="height: 100px; width: 100px; border-radius: 50px;">
	                        </div>
					    	<blockquote>
					    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
					    	</blockquote>
					    </div>
					    <div class="active item">
	                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);">
	                        	<img class="img-thumbnail" src="dist/images/mamun01.jpg" style="height: 100px; width: 100px; border-radius: 50px;">
	                        </div>
					    	<blockquote>
					    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
					    	</blockquote>
					    </div>
	                    <div class="item">
	                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);">
	                        	<img class="img-thumbnail" src="dist/images/mamun01.jpg" style="height: 100px; width: 100px; border-radius: 50px;">
	                        </div>
	    			    	<blockquote>
					    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
					    	</blockquote>
					    </div>
	                    <div class="item">
	                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);">
	                        	<img class="img-thumbnail" src="dist/images/mamun01.jpg" style="height: 100px; width: 100px; border-radius: 50px;">
	                        </div>
	    			    	<blockquote>
					    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
					    	</blockquote>
					    </div>
	                    <div class="item">
	                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);">
	                        	<img class="img-thumbnail" src="dist/images/mamun01.jpg" style="height: 100px; width: 100px; border-radius: 50px;">
	                        </div>
	    			    	<blockquote>
					    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
					    	</blockquote>
					    </div>
					  </div>
					</div>
				</div>							
			</div>
		</div>
	</section>

<?php include('partials/foot.php');?>
<?php include('partials/footer.php');?>

