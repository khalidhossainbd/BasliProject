	<section>
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-6">
								<img class="img-responsive" src="dist/images/logoBasli.png">
							</div>
							<div class="col-md-8 col-sm-6 col-xs-6">
								<p class="footer-head">Bangladesh Assocation of <br>
							Sign Language Interpreters</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<p class="footer-head">Contact Us</p>
						<hr class="footer-hr">
						<p class="footer-text">
							202,MuktoBanglaShopping complex<br>
							8th floor, Mirpur-1, Dhaka-1216 <br>
							Cell: 01919-839925
						</p>
					</div>
					<div class="col-md-4">
						<p class="footer-head">Importent Link</p>
						<hr class="footer-hr">
					</div>
				</div>
			</div>
		</div>
		<div class="footer-end">
			<p>Developed By: <a href="https://www.linkedin.com/in/khalidhossainbd/" target="_blank">Md Khalid Hossain</a></p>
		</div>
	</section>