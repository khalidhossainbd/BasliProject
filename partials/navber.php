
<section>
	<div class="container">
		<div class="top-head">
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6">
					<ul class="top-icon">
						<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<div class="logo-top">
						<img class="img-responsive logo" src="dist/images/logoBasli.png">
						</div>
					</div>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<p class="logo-text">Bangladesh Assocation of <br>Sign Language Interpreters</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div style="padding: 17px 0;">
				<p class="logo-text-2">"Committed to the development of <br>the profession of sign language <br> interpreting worldwide"
				<hr class="hr-class">
				</p>
				</div>

			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<nav class="navbar navbar-default">
	        <div class="navbar-header">
	         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	           <span class="sr-only">Toggle navigation</span>
	           <span class="icon-bar"></span>
	           <span class="icon-bar"></span>
	           <span class="icon-bar"></span>
	         </button>
	         <a class="navbar-brand" href="index.php">BASLI</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
		        <ul class="nav navbar-nav">
		           <li class="active"><a href="index.php">Home</a></li>
		           
		           <li class="dropdown">
		             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us<span class="caret"></span></a>
		             <ul class="dropdown-menu animated fadeInRight slow">
		               <li><a href="mission.php">Mission & Objectives</a></li>
		               <li role="separator" class="divider"></li>
		               <li><a href="history.php">History</a></li>
		               <li role="separator" class="divider"></li>
		               <li><a href="#">Executive Board</a></li>
		               <li role="separator" class="divider"></li>
		               <li><a href="#">Role of the Interpreter</a></li>
		               <li role="separator" class="divider"></li>
		               <li><a href="#">Partners of WASLI</a></li>
		               <li role="separator" class="divider"></li>
		               <li><a href="#">Achievement</a></li>
		             </ul>
		           </li>
		           <li><a href="#">Deaf Interpreters</a></li>
		           <!-- <li><a href="#">Gallery</a></li> -->
		           <li class="dropdown">
		             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gallery<span class="caret"></span></a>
		             <ul class="dropdown-menu animated fadeInRight slow">
		               <li><a href="imageGallery.php">Image Gallery</a></li>
		               <li role="separator" class="divider"></li>
		               <li><a href="#">Event Gallery</a></li>
		               <li role="separator" class="divider"></li>
		               <li><a href="#">Video Gallery</a></li>
		             </ul>
		           </li>
		           <!-- <li><a href="#"></a></li> -->
		           <li class="dropdown">
		             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Conference <span class="caret"></span></a>
		             <ul class="dropdown-menu animated fadeInRight slow">
		               <li><a href="#">Action</a></li>
		               <li><a href="#">Another action</a></li>
		               <li><a href="#">Something else here</a></li>
		               <li role="separator" class="divider"></li>
		               
		               <li><a href="#">Separated link</a></li>
		               <li><a href="#">One more separated link</a></li>
		             </ul>
		           </li>
		        </ul>
		        <ul class="nav navbar-nav navbar-right">
		           <li><a href="">Donor</a></li>
		           <li><a href="">Contact Us</a></li>
		        </ul>
	        </div>
		</nav>
	</div>
</section>