<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Basli | Bangladesh Assocation of Sign Language Interpreters</title>
  	<meta name="description" content="Bangladesh Assocation of Sign Language Interpreters">
  	<meta name="keywords" content="Basli, Sign Language, Sign Language Interpreters, Bangladesh Assocation of Sign Language Interpreters">
  	<meta name="author" content="A H Mamun">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="dist/images/logoBasli.png">
  	<link rel="stylesheet" type="text/css" href="dist/lib/bootstrap/css/bootstrap.css">
  	
  	<link rel="stylesheet" type="text/css" href="dist/lib/font-awesome/css/font-awesome.min.css">
  	<link rel="stylesheet" type="text/css" href="dist/lib/animate-css/animate.css">

  	<link rel="stylesheet" type="text/css" href="dist/css/custom.css">
  	<link rel="stylesheet" type="text/css" href="dist/css/style.css">
</head>
<body>