	<script type="text/javascript" src="dist/lib/jquery/jquery.js"></script>
	<script type="text/javascript" src="dist/lib/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="dist/js/myjs.js"></script>
	<script type="text/javascript">		
		$(document).ready(function () {
		$('.navbar .dropdown').hover(function () {
		        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
		    }, function () {
		        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
		    });
		});
	</script>
	<script type="text/javascript">
		$('.carousel').carousel({
		    interval: 5000
		}); 
	</script>
</body>
</html>