<?php include('partials/header.php');?>
<?php include('partials/navber.php');?>

<section>
	<div class="container">
		<div class="top-bg">
			<p class="top-text">Mission & Objectives</p>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<p class="con-head">The aim of BASLI is to advance the profession of sign language interpreting countrywise.</p>

				<p class="con-sub">In pursuit of the aim we will:</p>
				<ul class="con-list">
					<li>Encourage the establishment of national associations of sign language interpreters in countries that do not have them</li>
					<li>Be a support network for existing national associations of sign language interpreters</li>
					<li>Share information and be a reference point for interpreting issues, using the World Wide Web and other internationally accessible ways</li>
					<li>Support the work of sign language interpreters working at international events, e.g. conferences, sporting events</li>
					<li>Work in partnership with Deaf and Deafblind associations on sign language interpreting issues</li>
					<li>Encourage research</li>
					<li>Develop and promote standards for high quality training, education and assessment of sign language interpreters</li>
					<li>Host conferences and seminars</li>
					<li>Liaise with spoken language interpreter organisations and other organisations having common interests</li>
				</ul>
			</div>
		</div>
	</div>
</section>


<?php include('partials/foot.php');?>
<?php include('partials/footer.php');?>
