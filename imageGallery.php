<?php include('partials/header.php');?>
<?php include('partials/navber.php');?>

<section>
  <div class="container">
    <div class="top-bg">
      <p class="top-text">Image Gallery</p>
    </div>
  </div>
</section>

<section style="margin-bottom: 40px;">
  <div class="container">
    <div class="gallery">
      <figure>
        <img src="dist/images/gallery/gallery01.jpg" alt="" />
        <figcaption>Daytona Beach <small>United States</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery02.jpg" alt="" />
        <figcaption>Териберка, gorod Severomorsk <small>Russia</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery03.jpg" alt="" />
        <figcaption>
          Bad Pyrmont <small>Deutschland</small>
        </figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery04.jpg" alt="" />
        <figcaption>Yellowstone National Park <small>United States</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery05.jpg" alt="" />
        <figcaption>Quiraing, Portree <small>United Kingdom</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery01.jpg" alt="" />
        <figcaption>Highlands <small>United States</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery02.jpg" alt="" />
        <figcaption>Daytona Beach <small>United States</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery03.jpg" alt="" />
        <figcaption>Териберка, gorod Severomorsk <small>Russia</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery04.jpg" alt="" />
        <figcaption>
          Bad Pyrmont <small>Deutschland</small>
        </figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery05.jpg" alt="" />
        <figcaption>Yellowstone National Park <small>United States</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery01.jpg" alt="" />
        <figcaption>Quiraing, Portree <small>United Kingdom</small></figcaption>
      </figure>
      <figure>
        <img src="dist/images/gallery/gallery02.jpg" alt="" />
        <figcaption>Highlands <small>United States</small></figcaption>
      </figure>
    </div>
  </div>
</section>




<?php include('partials/foot.php');?>
<?php include('partials/footer.php');?>


<script>
    popup = {
  init: function(){
    $('figure').click(function(){
      popup.open($(this));
    });
    
    $(document).on('click', '.popup img', function(){
      return false;
    }).on('click', '.popup', function(){
      popup.close();
    })
  },
  open: function($figure) {
    $('.gallery').addClass('pop');
    $popup = $('<div class="popup" />').appendTo($('body'));
    $fig = $figure.clone().appendTo($('.popup'));
    $bg = $('<div class="bg" />').appendTo($('.popup'));
    $close = $('<div class="close"><svg><use xlink:href="#close"></use></svg></div>').appendTo($fig);
    $shadow = $('<div class="shadow" />').appendTo($fig);
    src = $('img', $fig).attr('src');
    $shadow.css({backgroundImage: 'url(' + src + ')'});
    $bg.css({backgroundImage: 'url(' + src + ')'});
    setTimeout(function(){
      $('.popup').addClass('pop');
    }, 10);
  },
  close: function(){
    $('.gallery, .popup').removeClass('pop');
    setTimeout(function(){
      $('.popup').remove()
    }, 100);
  }
}

popup.init()

</script>